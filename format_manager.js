
Drupal.behaviors.format_manager = function(context) {
  $('a[href=/admin/settings/filters/add]', context).not('.fm-processed').each(function() {
    $(this).addClass('fm-processed');
    href = $(this).attr('href');
    $(this).attr('href', href + '?destination=admin/settings/filters');
  });
}
  
